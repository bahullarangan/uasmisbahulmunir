import React from 'react';
import { Text, View,Image } from 'react-native';

const Syiir = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4", marginTop:50,alignContent:"center",alignItems:"center" }} >
            <Text style={{color:"#FFFFFF"}} >إِلٰـهِى لَسْتُ لِلْفِرْدَوْسِ أَهْلاً # وَلاَ أَقْوٰى عَلَى نَارِ الْجَحِيْمِ</Text>
            <Text style={{color:"#FFFFFF"}} >  فَهَبْ لِي تَوْبَةً وَاغْفِرْ ذُنُوْبِيْ # فَإِنَّكَ غَافِرُ الذَّنْبِ الْعَظِيْمِ</Text>
             <Text style={{color:"#FFFFFF"}} >     ذُنُوْبِيْ مِثْلُ أَعْدَادِ الرِّمَالِ # فَهَبْ لِيْ تَوْبَةً يَاذَاالْجَلاَلِى</Text>
             <Text style={{color:"#FFFFFF"}} >    وَعُمْرِي نَاقِصٌ فِي كُلِّ يَوْمٍ # وَذَنْبِي زَائِدٌ كَيْفَ احْتِمَالِي</Text>
             <Text style={{color:"#FFFFFF"}} >      إِلٰـهِي عَبْدُكَ الْعَاصِي أَتَاكَ # مُقِرًّا بِالذُّنُوْبِ وَقَدْ دَعَاكَ </Text>
               <Text style={{color:"#FFFFFF"}} >     فَإِنْ تَغْفِرْ فَأنْتَ لِذَاكَ أَهْلٌ # فَإِنْ تَطْرُدْ فَمَنْ نَرْجُو سِوَاكَ </Text>
            
        </View>
    )
}

export default Syiir;